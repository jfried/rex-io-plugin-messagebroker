#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::IO::Server::MessageBroker;
use Mojo::Base 'Mojolicious::Controller';

use Data::Dumper;
use Mojo::JSON "j";
use Mojo::UserAgent;
use Rex::IO::Server::Helper::IP;
use Mojo::Redis;
use Gearman::Client;
use JSON::XS;

sub broker {
}

sub __register__ {
  my ( $self, $app ) = @_;
  my $r = $app->routes;

  $r->websocket("/messagebroker")->over( authenticated => 1 )
    ->to("message_broker#broker");
}

1;
